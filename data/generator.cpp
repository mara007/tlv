#include "generator-defs.h"

void generate(const std::string &fileName, const std::vector<Item> items) {
    std::ofstream f(fileName + ".tlv", f.binary);
    for (auto &item : items)
        f << item;
}

int main() {
    generate("empty", {
        Item(1)
    });
    generate("hello", {
        Item(2, "hello")
    });
    generate("two-numbers", {
        Item(3, 3),
        Item(4, 0)
    });
    generate("double", {
        Item(8444, 1.51),
        Item(8884, 5.55)
    });
    generate("mix", {
        Item(18, "dear"),
        Item(0),
        Item(188, 0xBEEF),
        Item(0),
        Item(19, "grill"),
        Item(0),
        Item(20, "w"),
        Item(21, 8)
    });

    return EXIT_SUCCESS;
}