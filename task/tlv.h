#pragma once
// Declarations

#include <cstdint>
#include <optional>
#include <iostream>

#define TRC(x) std::cout << x << std::endl;

class tlv_t {
    const char * data;
    size_t size;

    bool check_size(const char *p, uint16_t s);

    public:

    using tag_id_t = uint16_t;
    using tag_size_t = uint16_t;

    tlv_t(const char * d, size_t s)
    : data(d), size(s)
    {}

    struct tag_t {
        tag_t(tag_size_t s, const char *d)
        : size(s), data(d)
        {}

        tag_size_t size;
        const char  *data;

        std::string s() const;
        int i() const;
        double d() const;
    };

    using result_t = std::optional<tag_t>;
    result_t get_tag(const tag_id_t tag_id);
};
