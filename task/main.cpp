#include <fstream>
#include <iostream>
#include <string>
#include <algorithm>

#include "tlv.h"

#define GET(x, t) { \
    auto ret = tvl.get_tag(t); \
    if (ret) { \
        TRC("TAG: " << (t)); \
        TRC("VAL:" << ret->x() << std::endl); \
    } else { \
        TRC("TAG: " << (t) << " NOT FOUND!"); \
    } \
}

bool match(const std::string &what, const std::string &with) {
    return std::string::npos != what.find(with);
}

int main(int argc, char **argv) {

    if (argc != 2) {
        std::cerr << "missing argument" << std::endl;
        return EXIT_FAILURE;
    }

    std::ifstream f(argv[1], f.in | f.binary);
    if (f.fail()) {
        std::cerr << "file " << argv[1] << " could not be opened" << std::endl;
        return EXIT_FAILURE;
    }
    std::string content{std::istreambuf_iterator<char>(f), std::istreambuf_iterator<char>()};

    // ...
    std::string file_name(argv[1]);
    tlv_t tvl(content.c_str(), content.size());

    TRC("checking " << file_name);

    if (match(file_name, "mix.tlv")) {
        GET(i, 10);
        GET(i, 21);
        GET(s, 20);
        GET(s, 19);
        GET(i, 188);
        GET(s, 18);
    }
    return EXIT_SUCCESS;
}

