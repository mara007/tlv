#include "tlv.h"

// Implementation

bool tlv_t::check_size(const char *p, uint16_t s) {
    return p + s <= data + size;
}

tlv_t::result_t tlv_t::get_tag(const tag_id_t tag_id) {
    TRC("parse start");
    const char * ptr = data;

    while (check_size(ptr, sizeof(tag_id_t) + sizeof(tag_size_t))) {
        const tag_id_t *curr_tag_id = (tag_id_t*)ptr;
        ptr += sizeof(tag_id_t);

        const tag_size_t *curr_tag_size = (tag_size_t*)ptr;
        ptr += sizeof(tag_size_t);

        TRC("got: tag: " << *curr_tag_id << ", tag size: " << *curr_tag_size);
        if (*curr_tag_id == tag_id) {
            if (check_size(ptr, *curr_tag_size)) {
                TRC("..ok");
                return tag_t(*curr_tag_size, ptr);
            }

            TRC("..wrong size");
            return {};
        }

        ptr += *curr_tag_size;
    }

    TRC("parse end, not found");
    return {};
}


std::string tlv_t::tag_t::s() const {
    TRC("..get string of size: " << size)
    return std::string(data, size);
}

int tlv_t::tag_t::i() const {
    TRC("..get int of size: " << size)
    return *reinterpret_cast<const int*>(data);
}

double tlv_t::tag_t::d() const {
    TRC("..get double of size: " << size)
    return *reinterpret_cast<const double*>(data);
}
